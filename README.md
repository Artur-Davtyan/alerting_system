## Alerting System 
 
### Short Description
_This project will solve your problem with alertings.
Alerting system consist of Prometheus, Alertmanager, Grafana and Mattermost. In this documentation you can find Alerting system components configuration and setup process._


### Prometheus Setup
_Visit [prometheus.io](https://prometheus.io/docs/introduction/overview/) for full documentation, examples and guides._

* For the first we need to pull docker prometheus image, docker image's tag we choose latest
  > ```$ docker pull prom/prometheus:latest```
* Until run docker command we must configuration _**prometheus.yml**_  and _**prometheus.rules.yml**_ files.
  > _prometehus.yml configuration file_
  * You will find Prometheus configuration in `/config/prometheus/prometheus.yml`.
  * Also you will set up you targets host ip and ports.
  > _prometheus.rules.yml configuration file_
  * You will find Prometheus rules configuration in `path_to/config/prometheus/prometheus.rules.yml`.
* Docker run command:
  >```$ docker run -d --name=prometheus -p 9090:9090 -v /config/prometheus:/etc/prometheus -v /data/promethus:/data prom/prometheus --restart=always   --config.file=/etc/prometheus/premetheus.yml```

### AlertManager Setup
_Visit [prometheus.io](https://prometheus.io/docs/alerting/overview/) for full documentation about alertmanager configuration, examples and guides._

* For the first we need to pull docker alertmanager image, docker image's tag we choose latest
  >```$ docker pull prom/alertmanager:latest```
* We must configuration _**alertmanager.yml**_ file.
  > _alermanager.yml condiguration file_
  * You will find Alermanager configuration in `/config/alermanager/alermanager.yml`.
* Docker run command:
  > ```$ docker run -d --name=alertmanager -p 9093:9093 -v /config/alermanager/:/etc/alertmanager -v /data/alertmanager:/data --restart=always  --config.file=/etc/alertmanager/alermanager.yml```
  
### Mattermost Setup
_Visit [docs.mattermost.com](https://docs.mattermost.com/guides/administrator.html#configure-mattermost) for full documentation about mattermost configuration, and incomming webhook creating see [docs.mattermost.com](https://docs.mattermost.com/developer/webhooks-incoming.html?highlight=webhook%20slack)._

* For the first we need to pull docker mattermost image, docker image's tag we choose latest
  > ```$ docker pull mattermost/mattermost-preview:latest  ```
* Docker run command:
  > ``` $ docker run -d --name=matermost-preview -p 8065:8065 -v /data/matermost:/mattermost/data mattermost/mattermost-preview --restart=always ```

*   After run mattermost server and set up it. You will create incoming webhook for your alerting channel and that url set up in alermanager.yml file.
  
### Grafana Setup
_Visit [grafana.com](https://grafana.com/docs/grafana/latest/installation/docker/) for full documentation about alertmanager configuration, examples and guides._

* For the first we need to pull docker grafana image, docker image's tag we choose 6.5.0
  > ``` $ docker pull grafana/grafana:6.5.0```
* Docker run command:
  > ``` $ docker run -d --name=grafana -p 3000:3000 -v /data/grafana:/var/lib/grafana --restart=always grafana/grafana:6.5.0 --restart=always```
* After run grafana and set up it. We connect prometheus to grafana.
* For see alert to grafana we use dashbord Prometheus Alerts that ID is 11098. [Link for dashboard!](https://grafana.com/grafana/dashboards/11098).